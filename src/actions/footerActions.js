import { ADD_NAME } from '../types';

export const addName = ( name, email ) => {
    const action = {
        type: ADD_NAME,
        addedName: name,
        addedEmail: email
    }
    return action;
}