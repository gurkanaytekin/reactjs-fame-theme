import axios from 'axios';
import { appVersion, serverURL } from '../constant';

class BaseHTTP {
    constructor() {
        this.baseURL = serverURL;
        axios.interceptors.request.use(function (config) {
            config.headers.common['appversion'] = appVersion;
            return config;
        }, function (error) {
            return Promise.reject(error);
        });
      
    }

    get(path) {
        let url = `${this.baseURL}${path}`
        return axios.get(`${url}`);
    }
    post(path, data) {
        let url = `${this.baseURL}${path}`
        return axios.post(`${url}`, data);
    }
}

export default (new BaseHTTP);