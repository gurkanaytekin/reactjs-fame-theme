import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header page-scroll">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand page-scroll" href="#page-top">Fame</a>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">
                            <li className="hidden">
                                <a href="#page-top"></a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#feature">Feature</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#portfolio">Portfolio</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#about-us">About</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#service">Services</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#team">Team</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#pricing">Pricing</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#latest-news">Latest News</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#testimonial">Testimonials</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#partner">Partner</a>
                            </li>
                            <li>
                                <a className="page-scroll" href="#contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Header;