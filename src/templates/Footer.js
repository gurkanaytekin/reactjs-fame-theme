import React from 'react';
import { connect } from 'react-redux';
import { addName } from '../actions/footerActions'

class Footer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            phone: '',
            message: ''
        };
    }
    
    updateInput = (e) => {
        this.setState({[e.target.id]: e.target.value});
    }

    render(){
        return(
            <section id="contact" className="contact">
                <div className="row">
                    <div className='col-lg-12'>
                        Kaydınız başarıyla alınmıştır. { this.props.addedName }, { this.props.addedEmail }
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="section-title text-center">
                                <h3 style={{display:'inline'}}>İsim:</h3> <h3> {this.state.name} </h3>
                                <h3 style={{display:'inline'}}>Eposta:</h3> <h3> {this.state.email} </h3>
                                <h3 style={{display:'inline'}}>Telefon:</h3> <h3> {this.state.phone} </h3>
                                <h3 style={{display:'inline'}}>Mesaj:</h3> <h3> {this.state.message} </h3>
                            </div>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="col-lg-12">
                            <form name="sentMessage" id="contactForm" noValidate>
                           
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input type="text" onChange={(e)=>this.updateInput(e)} className="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name." />
                                            <p className="help-block text-danger"></p>
                                        </div>
                                        <div className="form-group">
                                            <input type="email" onChange={(e)=>this.updateInput(e)} className="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address." />
                                            <p className="help-block text-danger"></p>
                                        </div>
                                        <div className="form-group">
                                            <input type="tel" onChange={(e)=>this.updateInput(e)} className="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number." />
                                            <p className="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <textarea onChange={(e)=>this.updateInput(e)} className="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                            <p className="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="col-lg-12 text-center">
                                        <div id="success"></div>
                                        <button type="button" onClick={()=>this.props.addName(this.state.name, this.state.email)} className="btn btn-primary">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="footer-contact-info">
                                <h4>Contact info</h4>
                                <ul>
                                    <li><strong>E-mail :</strong> your-email@mail.com</li>
                                    <li><strong>Phone :</strong> +8801-6778776</li>
                                    <li><strong>Mobile :</strong> +8801-45565378</li>
                                    <li><strong>Web :</strong> yourdomain.com</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-4 col-md-offset-4">
                            <div className="footer-contact-info">
                                <h4>Working Hours</h4>
                                <ul>
                                    <li><strong>Mon-Wed :</strong> 9 am to 5 pm</li>
                                    <li><strong>Thurs-Fri :</strong> 12 pm to 10 pm</li>
                                    <li><strong>Sat :</strong> 9 am to 3 pm</li>
                                    <li><strong>Sunday :</strong> Closed</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="style-1">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 col-xs-12">
                                <span className="copyright">Copyright &copy; <a href="http://guardiantheme.com">GuardinTheme</a> 2015</span>
                            </div>
                            <div className="col-md-4 col-xs-12">
                                <div className="footer-social text-center">
                                    <ul>
                                        <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i className="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-4 col-xs-12">
                                <div className="footer-link">
                                    <ul className="pull-right">
                                        <li><a href="#">Privacy Policy</a>
                                        </li>
                                        <li><a href="#">Terms of Use</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
        );
    }
}
const mapStateToProps = state => ({
    addedName: state.footerReducer.addedName,
    addedEmail: state.footerReducer.addedEmail
})

export default connect(mapStateToProps, { addName })(Footer);