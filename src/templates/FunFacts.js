import React from 'react';

class FunFacts extends React.Component {
    render() {
        return(
            <section className="fun-facts">
        <div className="container">
            <div className="row">
                <div className="col-xs-12 col-sm-6 col-md-3">
                      <div className="counter-item">
                        <i className="fa fa-cloud-upload"></i>
                        <div className="timer" id="item1" data-to="991" data-speed="5000"></div>
                        <h5>Files uploaded</h5>                               
                      </div>
                    </div>  
                    <div className="col-xs-12 col-sm-6 col-md-3">
                      <div className="counter-item">
                        <i className="fa fa-check"></i>
                        <div className="timer" id="item2" data-to="7394" data-speed="5000"></div>
                        <h5>Projects completed</h5>                               
                      </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-3">
                      <div className="counter-item">
                        <i className="fa fa-code"></i>
                        <div className="timer" id="item3" data-to="18745" data-speed="5000"></div>
                        <h5>Lines of code written</h5>                               
                      </div>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-3">
                      <div className="counter-item">
                        <i className="fa fa-male"></i>
                        <div className="timer" id="item4" data-to="8423" data-speed="5000"></div>
                        <h5>Happy clients</h5>                               
                      </div>
                    </div>
            </div>
        </div>
    </section>
        );
    }
}

export default FunFacts;