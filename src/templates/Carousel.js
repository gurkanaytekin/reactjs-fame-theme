import React from 'react';

class Carousel extends React.Component {
    render() {
        return (
            <section id="page-top">

                <div id="main-slide" className="carousel slide" data-ride="carousel">


                    <ol className="carousel-indicators">
                        <li data-target="#main-slide" data-slide-to="0" className="active"></li>
                        <li data-target="#main-slide" data-slide-to="1"></li>
                        <li data-target="#main-slide" data-slide-to="2"></li>
                    </ol>



                    <div className="carousel-inner">
                        <div className="item active">
                            <img className="img-responsive" src="images/header-bg-1.jpg" alt="slider" />
                            <div className="slider-content">
                                <div className="col-md-12 text-center">
                                    <h1 className="animated3">
                                        <span><strong>Fame</strong> for the highest</span>
                                    </h1>
                                    <p className="animated2">At vero eos et accusamus et iusto odio dignissimos<br /> ducimus qui blanditiis praesentium voluptatum</p>
                                    <a href="#feature" className="page-scroll btn btn-primary animated1">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div className="item">
                            <img className="img-responsive" src="images/header-back.png" alt="slider" />

                            <div className="slider-content">
                                <div className="col-md-12 text-center">
                                    <h1 className="animated1">
                                        <span>Welcome to <strong>Fame</strong></span>
                                    </h1>
                                    <p className="animated2">Generate a flood of new business with the<br /> power of a digital media platform</p>
                                    <a href="#feature" className="page-scroll btn btn-primary animated3">Read More</a>
                                </div>
                            </div>
                        </div>


                        <div className="item">
                            <img className="img-responsive" src="images//galaxy.jpg" alt="slider" />
                            <div className="slider-content">
                                <div className="col-md-12 text-center">
                                    <h1 className="animated2">
                                        <span>The way of <strong>Success</strong></span>
                                    </h1>
                                    <p className="animated1">At vero eos et accusamus et iusto odio dignissimos<br /> ducimus qui blanditiis praesentium voluptatum</p>
                                    <a className="animated3 slider btn btn-primary btn-min-block" href="#">Get Now</a><a className="animated3 slider btn btn-default btn-min-block" href="#">Live Demo</a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <a className="left carousel-control" href="#main-slide" data-slide="prev">
                        <span><i className="fa fa-angle-left"></i></span>
                    </a>
                    <a className="right carousel-control" href="#main-slide" data-slide="next">
                        <span><i className="fa fa-angle-right"></i></span>
                    </a>
                </div>
            </section>
        );
    }
}

export default Carousel;