import React from 'react';

class AboutSection2 extends React.Component {
    render() {
        return (
            <div className="about-us-section-2">
                <div className="container">
                    <div className="row">

                        <div className="col-md-6">
                            <div className="skill-shortcode">


                                <div className="skill">
                                    <p>Web Design</p>
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" data-percentage="60">
                                            <span className="progress-bar-span" >60%</span>
                                            <span className="sr-only">60% Complete</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="skill">
                                    <p>HTML & CSS</p>
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" data-percentage="95">
                                            <span className="progress-bar-span" >95%</span>
                                            <span className="sr-only">95% Complete</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="skill">
                                    <p>Wordpress</p>
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" data-percentage="80">
                                            <span className="progress-bar-span" >80%</span>
                                            <span className="sr-only">80% Complete</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="skill">
                                    <p>Joomla</p>
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" data-percentage="100">
                                            <span className="progress-bar-span" >100%</span>
                                            <span className="sr-only">100% Complete</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="skill">
                                    <p>Extension</p>
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" data-percentage="70">
                                            <span className="progress-bar-span" >70%</span>
                                            <span className="sr-only">70% Complete</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="col-md-6">
                            <div id="carousel-example-generic" className="carousel slide about-slide" data-ride="carousel">

                                <ol className="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>

                                <div className="carousel-inner">
                                    <div className="item active">
                                        <img src="images/about-01.jpg" alt="" />
                                    </div>
                                    <div className="item">
                                        <img src="images/about-02.jpg" alt="" />
                                    </div>
                                    <div className="item">
                                        <img src="images/about-03.jpg" alt="" />
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default AboutSection2;