import React from 'react';

class FeatureSection extends React.Component{
    render() {
        return(
            <section id="feature" className="feature-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="feature">
                            <i className="fa fa-magic"></i>
                            <div className="feature-content">
                                <h4>Web Design</h4>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor. reprehenderit</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="feature">
                            <i className="fa fa-gift"></i>
                            <div className="feature-content">
                                <h4>Graphics Design</h4>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor. reprehenderit</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="feature">
                            <i className="fa fa-wordpress"></i>
                            <div className="feature-content">
                                <h4>Wordpress Theme</h4>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor. reprehenderit</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 col-xs-12">
                        <div className="feature">
                            <i className="fa fa-plug"></i>
                            <div className="feature-content">
                                <h4>Wordpress Plugin</h4>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor. reprehenderit</p>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </section>
        );
    }
}

export default FeatureSection;