import React from 'react';

class PortofileModal extends React.Component{
    render() {
        return(
            <div className="section-modal modal fade" id="portfolio-modal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-content">
                <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                        <div className="rl">
                        </div>
                    </div>
                </div>
                
                <div className="container">
                    <div className="row">
                        <div className="section-title text-center">
                            <h3>Portfolio Details</h3>
                            <p>Duis aute irure dolor in reprehenderit in voluptate</p>
                        </div>
                    </div>
                    <div className="row">
                        
                        <div className="col-md-6">
                            <img src="images/portfolio/img1.jpg" className="img-responsive" alt=".." />
                        </div>
                        <div className="col-md-6">
                            <img src="images/portfolio/img1.jpg" className="img-responsive" alt=".." />
                        </div>
                        
                    </div>
                </div>                
            </div>
        </div>
        );
    }
}

export default PortofileModal;