import React from 'react';

class News extends React.Component {
    render() {
        return(
            <section id="latest-news" className="latest-news-section">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="section-title text-center">
                        <h3>Latest News</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate</p>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="latest-news">
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-01.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>31</strong> <br />Dec , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-02.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>17</strong> <br />Feb , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-03.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>08</strong> <br />Aug , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-01.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>08</strong> <br />Aug , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-02.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>08</strong> <br />Aug , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="latest-post">
                            <img src="images/about-03.jpg" className="img-responsive" alt="" />
                            <h4><a href="#">Standard Post with Image</a></h4>
                            <div className="post-details">
                                <span className="date"><strong>08</strong> <br />Aug , 2014</span>
                                
                            </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        );
    }
}

export default News;