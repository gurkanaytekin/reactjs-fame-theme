import React from 'react';

class Testimonial extends React.Component {
    render() {
        return (
            <div id="testimonial" className="testimonial-section">
                <div className="container">
                    <div id="testimonial-carousel" className="testimonials-carousel">
                        <div className="testimonials item">
                            <div className="testimonial-content">
                                <img src="images/testimonial/face_1.png" alt="" />
                                <div className="testimonial-author">
                                    <div className="author">John Doe</div>
                                    <div className="designation">Organization Founder</div>
                                </div>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque<br /> laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
                            </div>
                        </div>
                        <div className="testimonials item">
                            <div className="testimonial-content">
                                <img src="images/testimonial/face_2.png" alt="" />
                                <div className="testimonial-author">
                                    <div className="author">Jane Doe</div>
                                    <div className="designation">Lead Developer</div>
                                </div>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia<br /> consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            </div>
                        </div>
                        <div className="testimonials item">
                            <div className="testimonial-content">
                                <img src="images/testimonial/face_3.png" alt="" />
                                <div className="testimonial-author">
                                    <div className="author">John Doe</div>
                                    <div className="designation">Honorable Customer</div>
                                </div>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit<br /> anim laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Testimonial;