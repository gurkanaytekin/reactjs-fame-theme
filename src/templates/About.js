import React from 'react';
import BaseHTTP from '../services/api';

class About extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        BaseHTTP.post(`users/login`, {username: 'gurkan', password: '123456'}).then((res)=>{
            console.log(res.data);
        }).catch((err)=>{
            console.log(err.response);
        });
      } 
    render() {
        return (
            <section id="about-us" className="about-us-section-1">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-sm-12">
                            <div className="section-title text-center">
                                <h3>About Us</h3>
                                <p>Duis aute irure dolor in reprehenderit in voluptate</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">

                        <div className="col-md-4">
                            <div className="welcome-section text-center">
                                <img src="images/about-01.jpg" className="img-responsive" alt=".." />
                                <h4>Office Philosophy</h4>
                                <div className="border"></div>
                                <p>Duis aute irure dolor in reprehen derit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Lorem reprehenderit</p>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="welcome-section text-center">
                                <img src="images/about-02.jpg" className="img-responsive" alt=".." />
                                <h4>Office Mission & Vission</h4>
                                <div className="border"></div>
                                <p>Duis aute irure dolor in reprehen derit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Lorem reprehenderit</p>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="welcome-section text-center">
                                <img src="images/about-03.jpg" className="img-responsive" alt=".." />
                                <h4>Office Value & Rules</h4>
                                <div className="border"></div>
                                <p>Duis aute irure dolor in reprehen derit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Lorem reprehenderit</p>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
        );
    }
}

export default About;