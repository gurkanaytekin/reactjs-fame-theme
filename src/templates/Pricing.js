import React from 'react';

class Pricing extends React.Component {
    render() {
        return (
            <div id="pricing" className="pricing-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="col-md-12">
                                <div className="section-title text-center">
                                    <h3>Our Pricing Plan</h3>
                                    <p className="white-text">Duis aute irure dolor in reprehenderit in voluptate</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">

                        <div className="pricing">

                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Free</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Standard</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Premium</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Professional</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Premium</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="pricing-table">
                                    <div className="plan-name">
                                        <h3>Professional</h3>
                                    </div>
                                    <div className="plan-price">
                                        <div className="price-value">$49<span>.00</span></div>
                                        <div className="interval">per month</div>
                                    </div>
                                    <div className="plan-list">
                                        <ul>
                                            <li>40 GB Storage</li>
                                            <li>40GB Transfer</li>
                                            <li>10 Domains</li>
                                            <li>20 Projects</li>
                                            <li>Free installation</li>
                                        </ul>
                                    </div>
                                    <div className="plan-signup">
                                        <a href="#" className="btn-system btn-small">Sign Up Now</a>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

export default Pricing;