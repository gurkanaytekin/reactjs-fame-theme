import React from 'react';

class ClientAside extends React.Component {
    render() {
        return (
            <section id="partner">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="section-title text-center">
                                <h3>Our Honorable Partner</h3>
                                <p>Duis aute irure dolor in reprehenderit in voluptate</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="clients">

                            <div className="col-md-12">
                                <img src="images/logos/themeforest.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/creative-market.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/designmodo.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/creative-market.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/microlancer.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/themeforest.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/microlancer.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/designmodo.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/creative-market.jpg" className="img-responsive" alt="..." />
                            </div>

                            <div className="col-md-12">
                                <img src="images/logos/designmodo.jpg" className="img-responsive" alt="..." />
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ClientAside;