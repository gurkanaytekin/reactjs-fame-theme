import React from 'react';

class ActionSection extends React.Component {
    render() {
        return(
            <section className="call-to-action">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h1>Libero tempore soluta nobis est eligendi<br /> optio cumque nihil impedit minus id quod maxime <br />placeat facere possimus, omnis voluptas assumenda est</h1>
                            <button type="submit" className="btn btn-primary">Buy This Template</button>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ActionSection;