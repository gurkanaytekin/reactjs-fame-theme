import React from 'react';

class Portofilo extends React.Component {
    render() {
        return (
            <section id="portfolio" className="portfolio-section-1">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="section-title text-center">
                                <h3>Our Portfolio</h3>
                                <p>Duis aute irure dolor in reprehenderit in voluptate</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">

                            <ul id="portfolio-list">
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img1.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img2.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img3.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img4.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img5.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="portfolio-item">
                                        <img src="images/portfolio/img6.jpg" className="img-responsive" alt="" />
                                        <div className="portfolio-caption">
                                            <h4>Portfolio Title</h4>
                                            <a href="#portfolio-modal" data-toggle="modal" className="link-1"><i className="fa fa-magic"></i></a>
                                            <a href="#" className="link-2"><i className="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Portofilo;