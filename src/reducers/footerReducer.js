import { ADD_NAME } from '../types';

let initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case ADD_NAME:
            return { ...state, addedEmail: action.addedEmail, addedName: action.addedName };
        default:
        return state;
    }
}