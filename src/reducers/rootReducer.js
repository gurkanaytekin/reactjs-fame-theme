import { combineReducers } from 'redux';
import footerReducer from '../reducers/footerReducer';

export default combineReducers({
    footerReducer: footerReducer
})