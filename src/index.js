import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from './templates/Header';
import Footer from './templates/Footer';
import Carousel from './templates/Carousel';
import FeatureSection from './templates/FeatureSection';
import ActionSection from './templates/ActionSection';
import Portofilo from './templates/Portofilo';
import PortofiloModal from './templates/PortofiloModal';
import About from './templates/About';
import AboutSection2 from './templates/AboutSection2';
import FunFacts from './templates/FunFacts';
import Teams from './templates/Teams';
import Pricing from './templates/Pricing';
import News from './templates/News';
import Testimonial from './templates/Testimonial';
import ClientsAside from './templates/ClientsAside';
import { Provider } from 'react-redux';
import store  from './store';

export default class App extends Component {
    render() {
      return (
        <div>
          <Header />
          <Carousel />
          <ActionSection />
          <Portofilo />
          <PortofiloModal />
          <About />
          <AboutSection2 />
          <FeatureSection />
          <FunFacts />
          <Teams />
          <Pricing />
          <News />
          <Testimonial />
          <ClientsAside />
          <Footer />
        </div>
      );
    }
  }
  

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
